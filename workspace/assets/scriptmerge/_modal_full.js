jQuery(function($){
  modalFullJq = function(btn, type){
    var btn = $(btn);
    var ext = btn.data();
    var obj = btn.closest('.laymodal');
    var type = type;

    if(!ext.fullscreen){
      modalwindowFull(obj, btn, type);
    }else{
      modalZommOut(obj, btn, type);
    }
  }

  var originHeigth;

  //전체화면
  var modalwindowFull = function(obj, btn, type){
    obj.css({
      width :'100%',
    });

    originHeigth = originHeigth ? originHeigth : obj.find('.laymodal-content').height();

    if(type == 'blueprint') {
      obj.find('.blueprint-preview').css('height',$(window).height() - 265);
    } else {
      // obj.find('.laymodal-content').css('height',originHeigth);
      obj.find('.laymodal-content').css('height',$(window).height() - 125);
    }

    btn.data('fullscreen','true');
    btn.removeClass('laymodal-reduce');
    btn.addClass('laymodal-full');
    btn.text('팝업크기 축소');
  }

  //축소
  var modalZommOut = function(obj, btn, type){
    obj.css({
      width : '',
    });

    console.log(type);
    if(type == 'blueprint'){
      obj.find('.blueprint-preview').css('height','');
    } else {
      obj.find('.laymodal-content').css('height',originHeigth);
    }

    btn.removeClass('laymodal-full');
    btn.addClass('laymodal-reduce');
    btn.data('fullscreen','');
    btn.text('전체 화면 보기');
  }

  modalFullCloseJq = function(btn){
    var btn = $(btn);
    var obj = btn.closest('.laymodal');

    console.log($(btn));
    
    modalZommOut(obj,btn);
    lay_pop_close(obj,btn);
  }

  scrollBind($('.blueprint-list'));
});

function modalFull(btn, type){
  jQuery(function($){
    modalFullJq(btn, type);
  });
}

function modalFullClose(btn, type){
  jQuery(function($){
    modalFullCloseJq(btn, type);
  });
}