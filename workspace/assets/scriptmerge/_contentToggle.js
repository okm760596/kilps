function isClass(obj, className){
  var classList = obj.className.split(' ');
  var className = className;

  if(classList.indexOf(className) == -1){
    return false
  }else{
    return true;
  }
}

function contentToggle(obj,target){
  var obj = obj,
      target = document.querySelector('[data-toggle='+ target +']');
  
  // if(obj.)
  if(!isClass(obj,'active')){
    obj.classList.add('active');
    target.classList.add('active');
  }else{
    obj.classList.remove('active');
    target.classList.remove('active');
  }
}