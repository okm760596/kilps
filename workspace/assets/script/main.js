jQuery(function($){
  var btn = $('#js__mapSearch'); //지도검색 button
  var map = $('.main-map-cont'); //지도
  var searchFrm = $('.map-search'); //검색폼
  var visual = $('.main-visual');
  
  var maincont = $('.main-container'); /////////// ❌❌❌❌❌❌❌❌❌❌



  //검색폼열기
  var searchFrmOpen = function(){
    searchFrm.css('display','block');
    searchFrm.addClass('open-ani');
  }

  //검색폼닫기
  var searchFrmClose = function(){
    searchFrm.addClass('close-ani');
    setTimeout(function(){
      searchFrm.removeClass('close-ani open-ani');
      searchFrm.css('display','none');

      //지도의 모든 패널 닫음
      $('.js__search-notext').hide();
      $('.js__search-nodata').hide();
      $('.js__search-result').hide();

    },300);
  }

  var mapOpen = function(){
    map.css('height',$(window).height() - ($('.lay-gnb-wrap').outerHeight() + 50));
    setTimeout(function(){
      searchFrmOpen();
    },300);
  }

  var mapClose = function(){
    map.css('height',700);
    searchFrmClose();
    $('html').animate({
      scrollTop : 0
    },300);
  }

  //상단이미지 줄이기
  var visualClose = function(){
    openTop = $('.lay-hedaer-top').outerHeight();
    visual.css('margin-top', -402);

    $('html').animate({
      scrollTop : openTop
    },300);
  }

  //상단이미지 늘리기(원래크기로)
  var visualOpen = function(){
    visual.css('margin-top', 0);
  }

  var toggleCont = function(){
   $('.main-quickbar-left > button').on('click',function(){
    
    var type = $(this).data('type');

    if(type == 'map' && btn.hasClass('_locked')){
      if(!btn.hasClass('active')){
        visualClose();
        mapOpen();
        btn.addClass('active');
      }else{
        visualOpen();
        mapClose();
        btn.removeClass('active');
      }
    }

    $('.main-quickbar-left > button').removeClass('_locked');
    $(this).addClass('_locked');
    

    

    if(type === 'work'){
      visualClose();
      $('.main-map').hide();
      $('.workstatus').show();
    }else if(type == 'map'){
      $('.main-map').show();
      $('.workstatus').hide();
    }

    
   });
  }

  //검색 클릭 이벤트 생성
  var searchTrigger = function(){
    // if(!btn.hasClass('_locked')) return false;
    btn.on('click',function(){
      if(!btn.hasClass('active')){
        visualClose();
        mapOpen();
        btn.addClass('active');
      }else{
        visualOpen();
        mapClose();
        btn.removeClass('active');
      }
    });
  }

  //이건 일단 놔둬보자 쓸지도 몰라!!!! 아마도!
  var scrollBg = function(){
   var bg = $('.main-visual .__bg');
   $(window).on('scroll',function(){
     console.log('a');
   });
  }


  var init = function(){
    // searchTrigger();
    toggleCont();
  }

  init();
  
});




//지도검색
jQuery(function($){

  var frm = $('.map-search-frm');
  var searchText = frm.find('.__text');

  var searchClear = function(){
    $('.js__search-notext').hide();
    $('.js__search-nodata').hide();
    $('.js__search-result').hide();
  }
  
  var search = function(){
    $('.map-search-frm .__btn-search').on('click',function(){
      var val = searchText.val(); //검색어
      searchClear();
      if(!val){
        $('.js__search-notext').show();
      }else if(val !== '신장동'){
        $('.js__search-nodata').show();
      }else{
        $('.js__search-result').show();
      }
    });
  }

  var addMethodSeelct = function(){
    frm.find('.__selector input').on('change',function(){
      var t =frm.find('.__selector input:checked').val();
      if(t == 'number'){
        searchText.attr('placeholder',"찾는 위치의 주소를 입력하세요. (예. 신장동 38-1)");
      }else if(t == 'road'){
        searchText.attr('placeholder',"찾는 위치의 주소를 입력하세요. (예. 도움6로 11)");
      }
    });
  }

  var resultTrigger = function(){
    $('.map-search-result button').on('click',function(){
      $('.map-maker').show();
      $('.js__search-result').hide();
    });
  }

  var searchTextDel = function(){
    searchText.on('input',function(){
      var val = $(this).val();
      if(val){
        frm.find('.__textbox').addClass('-istext');
      }else{
        frm.find('.__textbox').removeClass('-istext');
      }
    });

    frm.find('.__btn-textdel').on('click', function(){
      searchText.val('');
      searchText.focus();
      frm.find('.__textbox').removeClass('-istext');
    });
  }

  var init = function(){
    search();
    addMethodSeelct();
    resultTrigger();
    searchTextDel();
  }

  init();
});