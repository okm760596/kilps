//_accessibility.
jQuery(function($){
  $('.lay-sitejump a').on('click',function(){
    var targetText = $(this).attr('href');
    var target = $($(this).attr('href'));

    if(targetText == '#laycontent'){
      if($('.main-visual').length > 0){
        target = $('.main-visual');
      }else{
        target = $('.container');
      }
    }
    
    target.attr('tabindex', '0').focus();
    target.on('focusout',function(){
      target.removeAttr('tabindex');
    });
  });
});
//도면보기
jQuery(function($){

});
jQuery(function($){
  var memberSummary = function(){
    $('.lay-member-summary .summary-col > a').each(function(){
      var link = $(this);
      var pobj = link.parent('.summary-col');
      var pop = link.next('.summary-info');
      var setTime;

      link.on('mouseenter',function(){
        clearTimeout(setTime);
        pobj.removeClass('active');  
        pop.removeClass('slideUp slideDown');

        pobj.addClass('active');
        pop.addClass('slideDown');
      });

      pobj.on('mouseleave',function(){
        pop.addClass('slideUp');
        
        setTime = setTimeout(function(){
          pobj.removeClass('active');  
          pop.removeClass('slideUp slideDown');
        },300);
      });
    });
  }

  var working = function(){

    var workingObj = $('.js__working');
    if(workingObj.length < 1) return false;
    
    var elm = '';
    elm += '<div style="font-size:30px;font-weight:100;text-align: center;padding:60px 0;">';
    elm += 'Working <span style="width:10px;display:inline-block"></span> ';
    elm += '<span class="working"></span> ';
    elm += '<span class="working"></span> ';
    elm += '<span class="working"></span> ';
    elm += '<span class="working"></span> ';
    elm += '</div>';

    workingObj.html(elm);
  }

  var init = function(){
    memberSummary();
    working();
  }

  scrollBindJq = function(obj){
    obj.mCustomScrollbar({
      theme:"dark",
      scrollInertia:100,
      axis: 'y',
      mouseWheel: {
        // enable: false,
        preventDefault: true,
      }
    });
  }

  scrollBindJq($('.js__table-scroll'));
  
  init();
});

function scrollBind(obj){
  jQuery(function($){
    scrollBindJq(obj);
  });
}


function pageLoader(){
  var loader = '<div class="page-loader"><div><div class="page-loader-loding"></div> <div class="pager-loader-alert">국토이용정보통합플랫폼</div></div></div>';
  $('body').prepend(loader);
  $('body').css('overflow','hidden');
}

function pageLoaderComplete(){
  $('.page-loader').remove();
  $('body').css('overflow','');
}
function isClass(obj, className){
  var classList = obj.className.split(' ');
  var className = className;

  if(classList.indexOf(className) == -1){
    return false
  }else{
    return true;
  }
}

function contentToggle(obj,target){
  var obj = obj,
      target = document.querySelector('[data-toggle='+ target +']');
  
  // if(obj.)
  if(!isClass(obj,'active')){
    obj.classList.add('active');
    target.classList.add('active');
  }else{
    obj.classList.remove('active');
    target.classList.remove('active');
  }
}
jQuery(function($){
  // var datepicker = $("#datepicker");
  if(!$.datepicker) return false;
  var datePicker;
  var today = new Date();
  var datePickerHtml = '';
      datePickerHtml += '<div class="modal-datepicker">';
      datePickerHtml += '<div class="modal-datepicker_wrap">';
      datePickerHtml += '<div id="datepicker" class="datepicker">';
      datePickerHtml += '<button type="button" class="modal-datepicker_close">닫기</button>';
      datePickerHtml += '</div>';
      datePickerHtml += '</div>';
      datePickerHtml += '</div>';

  var createDatePicker = function(){
    datepicker.datepicker({
      onSelect: function(date, datepicker){
          obj = $('#'+$('.modal-datepicker').data('obj'));
          obj.val(date);
          $('.modal-datepicker').hide();
      }
    });
  }

  var callDatePicker = function(){
    // $('.modal-calendar').click(function(){

      $('body').append(datePickerHtml);
      datePicker = $("#datepicker");

      datepicker.datepicker();
      // createDatePicker();


      var datepickerWrap = $('.modal-datepicker');
      var input = $(this).prev();
      var sDate = new Date(input.val());
      var minDate = input.date;
  
      datepickerWrap.show();
      datepickerWrap.data('obj',input.attr('id'));
  
      datepicker.datepicker("setDate",sDate);
  
      if(input.data('minDate') != 'N'){
          datepicker.datepicker("option",{
              // minDate: today
          });
      }
    // });  
  }
  
  var dateInit = function(){
    $.datepicker.setDefaults({
      dateFormat: 'yy-mm-dd',
      prevText: '이전 달',
      nextText: '다음 달',
      monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
      monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
      dayNames: ['일', '월', '화', '수', '목', '금', '토'],
      dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
      dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
      showMonthAfterYear: true,
      yearSuffix: '',
    });
  }

  var dateRange = function(){
    var dateRangSet = $('.cs-datepicker-set');
    dateRangSet.each(function(){
      var row = $(this);
      var from = row.find('.js__date-from');
      var to = row.find('.js__date-to');

      // from.datepicker("setDate",new Date());
      // to.datepicker("setDate","+ 1w");

      from.datepicker({
        // defaultDate: "+1w",
        changeYear: true,
        numberOfMonths: 3,
      })
      .on( "change", function() {
        // console.log('나 바꼈다');
        to.datepicker( "option", "minDate", getDateRang( this ) );
      });

      to.datepicker({
        // defaultDate: "+1w",
        changeYear: true,
        numberOfMonths: 3
      }).on( "change", function() {
        from.datepicker( "option", "maxDate", getDateRang( this ) );
      });

    });
  }

  function getDateRang( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( "yy-mm-dd", element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }

  var init = function(){
    //한글설정
    dateInit();

    $('.js__datepicker').datepicker({
      changeMonth: false,
      changeYear: true
    });

    $('.modal-calendar').on('click',function(){
      
    });
    // callDatePicker();


    dateRange();
  }

  init();

  

  

  

  $('.modal-datepicker_close, .modal-datepicker_wrap').click(function(){
      $('.modal-datepicker').hide();
  });
  $('.datepicker').click(function(e){
      e.stopPropagation();
  });

  //date
  getDate = function(date){
    if(!date) date = new Date();
    var y = date.getFullYear();
        m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
        d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

        return y + '-' + m + '-' + d;
  }
});
jQuery(function($){
  

  //클립보드 복사
  urlCopy = function(text){
      $('body').append('<input type="text" id="clip_hidden_text" value="'+text+'" style="position:absolute;opacity:0;">');
      $('#clip_hidden_text').select();

      var successful = document.execCommand('copy');

      $('#clip_hidden_text').remove();

      if(!successful){
          return false;
      }else{
          return true;
      }
  }
});

function openPop(url,wname,width,height,event){
    event.preventDefault();
    var left = screen.availWidth/2 - width/2;
    if(height == 'auto'){
        var top = 30;
        height = screen.availHeight-130;
    }else{
        var top = screen.availHeight/2 - height/2;
    }
    var wopen = window.open(url,wname,'width='+width+', height='+height+',top='+top+',left='+left+',scrollbars,resizable=no');
}


function getBytes(str){
    var cnt = 0;
    for(var i =0; i<str.length;i++) {
        cnt += (str.charCodeAt(i) >128) ? 2 : 1;
    }
    return cnt;
}

function substrCut(str, lengthInBytes) {
    var resultStr = '';
    var startInChars = 0;
    var startInBytes = 0;

    for (bytePos = 0; bytePos < startInBytes; startInChars++) {
        ch = str.charCodeAt(startInChars);
        if(isNaN(ch)){
            break;
        }
        bytePos += (ch < 128) ? 1 : 2;
    }

    end = startInChars + lengthInBytes;

    for (n = startInChars; startInChars < end; n++) {
        ch = str.charCodeAt(n);
        if(str[n]==undefined) {
            break;	        	
        }
        end -= (ch < 128) ? 1 : 2;

        resultStr += str[n];
    }

    return resultStr;
}

function numberFormat(inputNumber) {
    return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
 }
jQuery(function($){
  var gnbWrap = $('.lay-gnb');
  var gnb = $('.lay-gnb-content');
  var header = $('.lay-hedaer');
  var gnb1depth = gnb.find('.active[class*=gnb-depth]');
  var gnb2depth = gnb.find('ul ul .active');
  
  var gnbHover = function(){
    gnb.on('mouseenter focusin',function(){
      header.addClass('gnb-open');
    });
  }

  var gnbLeave = function(){
    gnbWrap.on('mouseleave focusout',function(){
      gnb.find('.gnb-depth1').removeClass('active');
      header.removeClass('gnb-open');
    });
  }
  
  var gnbActive = function(){
    gnb.find('a').on('mouseenter focusin',function(){
      gnb.find('li').removeClass('active');
      $(this).closest('li').addClass('active');
      $(this).closest('[class*=gnb-depth]').addClass('active');
    });

    gnb.find('>ul>li>a').on('touchstart', function(e){
      e.preventDefault();
      if(!header.hasClass('gnb-open')){
        header.addClass('gnb-open');
      }else{
        header.removeClass('gnb-open');
      }
    });
  }

  var init = function(){
    gnbHover();
    gnbLeave();
    gnbActive();
  }

  init();
});
jQuery(function($){
  inputFile = $('input[class*=cs-file]');

  var fileTrigger = function(){
    inputFile.each(function(){
      var file = $(this);
      var fileID = file.attr('id');
      var label = $('[for=' + fileID + ']');
      var objWrap = file.closest('.cs-file-wrap');
      
      objWrap.find('.js__filename').html(file.attr('placeholder'));
      
      file.on('change',function(){
        var fileName = $(this).val().split('\\');

        fileName = fileName[fileName.length-1];
        objWrap.find('.js__filename').html(fileName);

        if(!fileName){
          objWrap.find('.js__filename').html(file.attr('placeholder'));
        }
      });
    });
  }

  var init = function(){
    if(inputFile.length < 1) return false;
    fileTrigger();
  }

  init();
});
// modal.js
// ver 1.3
jQuery(function($){

  // $('body').on('keyup', function(e){
  //   if(e.key === "Escape") {
  //     lay_pop_close();
  //   }
  // });

  var bgCloseAni;

  var bgOpen = function(){
    clearTimeout(bgCloseAni);
    var $popbg = $('#jq_modal_bg');
    $('#jq_modal_bg').removeClass('jq_modal_bg_close');

    if($popbg.length == 0){
      $('body').prepend('<div id="jq_modal_bg"></div>');
    }else{
      $popbg.css('display','block');
    }
  }

  var bgClose = function(){
    $('#jq_modal_bg').addClass('jq_modal_bg_close');

    bgCloseAni = setTimeout(function(){
      $('#jq_modal_bg').removeClass('jq_modal_bg_close');
      $('#jq_modal_bg').css('display','none');
      $('#jq_modal_bg').unbind();
      $('html').attr('style','');
    },300);
  }


  lay_pop_close = function(elm){
    var $obj;

    if(elm){
      if(typeof(elm) == 'string'){
        $obj = $(elm);
      }else{
        $obj = elm;
      }
    }else{
      $obj = $('.jq_modal');
    }

    $obj.removeClass('modal_open').addClass('modal_close');

    $carea = $('*[jq-carea='+$obj.data('jqModalarea')+']');
    $carea.focus();

    var activeModal = $('.jq_modal_wrap:visible');
    if(activeModal.length <= 1){
      bgClose();
    }

    setTimeout(function(){
      $obj.removeClass('modal_close');
      $obj.closest('.jq_modal_wrap').css('display','none');
      $obj.unbind();
    },300);
  }

    //lay_pop_open(object, button , close action)
    lay_pop_open = function(obj,t,bgclose){
      // if(bgclose != false) bgclose = false;
      bgclose = false;
      var $t = t; //회귀
      var $obj = obj; //mdoal
      var $bgClose = bgclose; //배경클릭시 모달 닫기기능 활성화 여부 true: 활성, false: 비활성

      if(!$obj){
        //$obj없을경우, href or jq_target 에 있는 정보를 사용함
        //target object
        $obj = $($t.attr('href'));
        //링크 사용한하고 jq_target으로 사용
        if($obj.length == 0)  $obj = $($t.attr('jq-target'));
      }

      if(typeof($obj) === 'string'){
        $obj = $(obj);
      }

      //modal없을때
      if($obj.length == 0) {
        console.error(obj+' 를 찾을수 없음');
        return false;
      }

      if($t){
        var $carea = $t.attr('id');

        $carea = new Date();
        $carea = $carea.getTime();
        $t.attr('jq-carea',$carea);
        $obj.data('jq-modalarea',$carea);
      } 

      //modal default setting
      $obj.data('jqModalBgclose', $bgClose);

      //현재 활성화된 modal
      var activeModal = $('.jq_modal_wrap:visible');
      var activeIdx;
      if(activeModal.length > 0){
        activeIdx = activeModal.css('z-index');
      }

      //background add
      bgOpen();

      isCreate = $obj.data('jq-modal') == 'create' ? true : false;
        
      if(!isCreate){
        $obj.data('jq-modal','create');
        $('body').prepend($obj);
        $obj.wrap('<div class="jq_modal_wrap"><div><div>');
        $obj.css('display','block');
        $obj.addClass('modal_open jq_modal');

        $obj.closest('.jq_modal_wrap').css('z-index',activeIdx + 1);

        //modal에서 포커스 나갔을때 event add
        modalFocusOutClose($obj);
      }else{
        pobj = $obj.closest('.jq_modal_wrap');
        pobj.css('display','block');
        $obj.data('jq-modalarea',$carea);
        $obj.removeClass('modal_close');
        $obj.addClass('modal_open');

        $obj.closest('.jq_modal_wrap').css('z-index',activeIdx + 1);
      }

      $obj.find('.slick-slider').resize();
      
      if($bgClose)
      {
        //배경클릭시 모달 닫기
        $('.jq_modal_wrap').click(function(){
            lay_pop_close();
        });
      }


      $obj.click(function(e){
        e.stopPropagation();
      });
        
      $('html').css({overflow: 'hidden'});

      $obj.attr('tabindex', '0');
      $obj.focus();
      $obj.focusout(function(){
          $obj.removeAttr('tabindex');
      });
    }

    //포커스가 나갈때 모달창 닫기
    var modalFocusOutClose = function($obj){
      var btn = $obj.find('button[jq-action=closeModalFocus]'),
          btns;
      var modal ={
        config: $obj.data()
      }

      //btn 생성
      if(btn.length < 1){
        btn = $('<button type="button" class="laymodal-focusclose" jq-action="closeModalFocus">');
        $obj.after(btn);
        $obj.before(btn.clone());
        btns = $obj.closest('.jq_modal_wrap').find('button[jq-action=closeModalFocus]');
      }

      if(modal.config.jqModalBgclose){
        btns.focusin(function(){
          lay_pop_close($(this).closest('.jq_modal_wrap').find('.jq_modal'));
        });
      }else{
        btns.focusin(function(){
          $obj.attr('tabindex','0').focus();
        });
      }
    }


    lay_pop_width = function(obj, width){
      $(obj).css('width', width);
    }


	$('*[jq-action="modal"]').click(function(e){
    e.preventDefault();
    lay_pop_open('',$(this));
  });

	$('*[jq-action="closeModal"]').click(function(e){
    e.preventDefault();
    lay_pop_close($(this).closest('.jq_modal'));
  });
});


var lay_pop_open = function(obj,t,bgclose){
  jQuery(function($){
    lay_pop_open(obj,t,bgclose);
  });
}

var lay_pop_close = function(){
  jQuery(function($){
    lay_pop_close();
  });
}

var lay_pop_width = function(obj, width){
  jQuery(function($){
    lay_pop_width(obj, width);
  });
}

jQuery(function($){
    //modal 창이 넘칠 경우 인데 이럴 경 우가 있나??
    var exresize = function(){
        obj = $('.laypop-webview-gallery').closest('.laypop');
        
        obj.css('width','1200px');
        $(window).resize(function(){
            a = $(window).width()-60;
            obj.css({width: a});
        });
    }

    var init = function(){
        if($('.laypop-webview-gallery').length > 0){
            exresize();
        }
    }

    // init();
});



//드래그모달
modaldrag = function(elm){
  jQuery(function(){
    function getzindex(){
      var maxIdx = 0;
      var allobj = $('.js__modaldrag:visible');
      allobj.each(function(idx,modalobj){
        var modalobj = $(modalobj);
        var zidx = Number(modalobj.css('z-index'));
        if(maxIdx < zidx) maxIdx = zidx;
      });
      return maxIdx;
    }

    var drage = false;
    var obj = $(elm);
    var objWidth = obj.outerWidth();
    var objHeight = obj.outerHeight();
    var wHeight;
    var wWidth;
    var header = obj.find('.laymodal-header');

    var getWindowSize = function(){
      wHeight = $(window).height();
      wWidth = $(window).width();
      console.log('a');
    }

    $(window).on('resize', function(){
      getWindowSize();
    });
    // getWindowSize();

    obj.addClass('js__modaldrag');

    obj.css('z-index', getzindex()+1);

    //처음좌표
    var originX;
    var originY;

    //클릭시좌표
    var clickX;
    var clickY;

    header.on('mousedown',function(e){
      originX = obj.offset().left;
      originY = obj.offset().top;
      clickX = e.clientX;
      clickY = e.clientY;

      obj.css('z-index', getzindex()+1);

      $(window).on('mousemove',function(e){
          moveX = originX + (e.clientX - clickX);
          moveY = originY + (e.clientY - clickY);
          // if(moveX < 0 || moveX > wWidth - objWidth) return false;
          // if(moveY < $(window).scrollTop() || moveY + objHeight >  $(window).scrollTop() + $(window).height()) return false;
          obj.css({
            left: moveX,
            top: moveY
          });
      });
    });

    

    $(window).on('mouseup',function(){
      $(window).off('mousemove');
      drage = false;
    });
  });
}

lay_pop_open_darge = function(elm, opt, button){
  jQuery(function(){
    var obj = $(elm); // modal
    var btn = $(button); // click button
    var btnxy = btn.offset();
    var objLeft;
    var objRight;

    var option = {
      width: 700, // 가로
      height: 400, // 높이
      left: 0, // 클릭한 버튼에서의 left 10px; , 버튼 좌 상단의 좌표기준
      top: btn.outerHeight() + 10, // 클릭한 버튼에서의 top 10px;
      center: false  // true 일때 가운데로 맞춤
    }

    $.extend(option, opt);

    $('body').prepend(obj);
    // console.log(obj);

    objContent = obj.find('.laymodal-content > div');
    objContent.css({
      height: option.height,
      overflowY: 'auto'
    });

    if(!option.center){
      objLeft = btnxy.left + option.left;
      objRight = btnxy.top + option.top;
    }else{
      objLeft = $(window).width()/2 - option.width/2;
      objRight = $(window).height()/2 - obj.height()/2 + $(window).scrollTop();
    }

    obj.css({
      display: 'block',
      position: 'absolute',
      zIndex: 2000,
      left: objLeft,
      top: objRight,
      transition: 'none',
      width: option.width
    });

    modaldrag(obj);
  });
}

lay_pop_close_darge = function(elm){
  jQuery(function($){
    obj = $(elm);
    obj.hide();
  });
}
jQuery(function($){
  modalFullJq = function(btn, type){
    var btn = $(btn);
    var ext = btn.data();
    var obj = btn.closest('.laymodal');
    var type = type;

    if(!ext.fullscreen){
      modalwindowFull(obj, btn, type);
    }else{
      modalZommOut(obj, btn, type);
    }
  }

  var originHeigth;

  //전체화면
  var modalwindowFull = function(obj, btn, type){
    obj.css({
      width :'100%',
    });

    originHeigth = originHeigth ? originHeigth : obj.find('.laymodal-content').height();

    if(type == 'blueprint') {
      obj.find('.blueprint-preview').css('height',$(window).height() - 265);
    } else {
      // obj.find('.laymodal-content').css('height',originHeigth);
      obj.find('.laymodal-content').css('height',$(window).height() - 125);
    }

    btn.data('fullscreen','true');
    btn.removeClass('laymodal-reduce');
    btn.addClass('laymodal-full');
    btn.text('팝업크기 축소');
  }

  //축소
  var modalZommOut = function(obj, btn, type){
    obj.css({
      width : '',
    });

    console.log(type);
    if(type == 'blueprint'){
      obj.find('.blueprint-preview').css('height','');
    } else {
      obj.find('.laymodal-content').css('height',originHeigth);
    }

    btn.removeClass('laymodal-full');
    btn.addClass('laymodal-reduce');
    btn.data('fullscreen','');
    btn.text('전체 화면 보기');
  }

  modalFullCloseJq = function(btn){
    var btn = $(btn);
    var obj = btn.closest('.laymodal');

    console.log($(btn));
    
    modalZommOut(obj,btn);
    lay_pop_close(obj,btn);
  }

  scrollBind($('.blueprint-list'));
});

function modalFull(btn, type){
  jQuery(function($){
    modalFullJq(btn, type);
  });
}

function modalFullClose(btn, type){
  jQuery(function($){
    modalFullCloseJq(btn, type);
  });
}
jQuery(function(){
  var toggleBox = $('[data-togglesection]');
  var idx = 1;
  var first = true;
  var btnNext = $('.js__togglesection-next');
  var btnPrev = $('.js__togglesection-prev');
  var btnTemp = $('.js__togglesection-tempsave');
  var btnSubmit = $('.js__togglesection-submit');
  var tab = $('.js__tablocation');
  var totallength = $('.js__tablocation li').length;

  var sectionGo = function(nextidx){
    toggleBox.hide();
    var nextidx =  nextidx;

    if(nextidx > totallength-1){
      btnNext.hide();
      btnSubmit.show();
      idx = totallength;
    }else{
      btnNext.show();
      btnSubmit.hide();
    }

    if(nextidx <= 1){
      idx = 1;
      btnPrev.hide();
      btnTemp.hide();
    }else{
      btnPrev.show();
      btnTemp.show();
    }

    toggleBox.each(function(nextidx){
      if($(this).data('togglesection') == idx){
        $(this).show();
      }
    });

    if(!first){
      $(window).scrollTop(tab.offset().top);
    }

    first = false;

    tabToogle();
  }

  var tabToogle = function(){
    tab.find('li').removeClass('locked active');
    tab.find('li').eq(idx-1).addClass('locked');
    tab.find('li').each(function(liidx){
      $(this).addClass('active');
      if(liidx-1 == idx-2) return false;
    });
  }

  var btnTrigger = function(){
    $('.js__togglesection-prev').on('click',function(){
      idx--;
      sectionGo(idx);
    });
    $('.js__togglesection-next').on('click',function(){
      idx++;
      sectionGo(idx);
    });
  }

  var init = function(){
    if(toggleBox.length < 1) return false;
    sectionGo(idx);
    btnTrigger();
  }

  init();
});

//_selectbox
jQuery(function($){
  var selectinit = function(){
    
    $('.cs-select').each(function(){
      var height = $(this).find('> button').outerHeight();
      var len = $(this).find('ul > li').length;
      var top = height*len*-1-10;

      $(this).find('ul').css('top', top);
      $(this).find('ul').data('top', top);
    });
  }

  var createSelectBox = function(){
    var select = $('.js__cs-select');

    select.each(function(){
      var item = $(this);
      var title = item.find('option:selected').html();
      var selectHtml = '';
      var selected = item.find('option:selected').index();
      var exclass = item.data('class') ? item.data('class') : '';
      var id = item.attr('id');

      if(!title){
        title = select.fine('option').eq(0).html();
      }

      selectHtml += '<div class="cs-select ' + exclass + '" data-id="'+ id +'">';
      selectHtml += '<button type="button">' + title + '</button>';
      selectHtml += '<div>';
      selectHtml += '<ul>';

      item.find('option').each(function(idx){
        if(idx == selected){
          selectHtml += '<li class="active"><button>' + $(this).html() + '</button></li>';
        }else{
          selectHtml += '<li><button>' + $(this).html() + '</button></li>';
        }
      });

      selectHtml += '</ul>';
      selectHtml += '</div>';
      selectHtml += '</div>';

      item.after(selectHtml);

    });
  }

  var selectFold = function(){
    $('.cs-select').removeClass('active');
    $('.cs-select').each(function(){
      $(this).find('ul').css('top', $(this).find('ul').data('top'));
    });
  }

  var selectBox = function(){
    $('body').on('click',function(){
      selectFold();
    });

    $('.cs-select').on('click',function(e){
      e.preventDefault();
      e.stopPropagation();
    });

    $('.cs-select > button').on('click',function(){
      var parent = $(this).parent('div');
      
      

      if(!parent.hasClass('active')){
        selectFold();

        $(this).parent('div').addClass('active');
        parent.find('ul').css('top',0);
      }else{
        $(this).parent('div').removeClass('active');
        parent.find('ul').css('top',parent.find('ul').data('top'));
      }
    });
  }

  var selectBoxChange = function(){
    $('.cs-select li button').on('click',function(e){
      e.preventDefault();
      var item = $(this).closest('.cs-select');
      var idx = $(this).parent().index();
      var select = $('#'+item.data('id'));

      select.val(select.find('option').eq(idx).val());

      item.find('> button').html($(this).text());
      selectFold();
      $(this).parent().addClass('active');
      
      item.removeClass('active');
    
    });
  }

  var init = function(){
    //셀렉트박스 생성
    createSelectBox();

    selectinit();

    //이벤트생성
    selectBox(); //셀레트박스 눌렀을때
    selectBoxChange(); //셀러트박스 OPTION 누렀을때
  }

  init();
});
jQuery(function(){
  var floatingHeadObj = $('.js__floatingHead');

  //이게 먼지 정확히 모르겠어
  // var triggerScroll = function(obj){
  //   obj.scroll(function(){
  //     var head = $(this).find('thead');
  //     var ttop = $(this).scrollTop();
  //     head.css('top',ttop);
  //   });
  // }

  // scrollBind = function(obj){
  //   obj.mCustomScrollbar({
  //     theme:"dark",
  //     scrollInertia:100,
  //     axis: 'y',
  //     mouseWheel: {
  //       // enable: false,
  //       preventDefault: true,
  //     }
  //   });
  // }

  var init = function(){
    if(floatingHeadObj.length < 1) return false;

    floatingHeadObj.each(function(){
      var obj = $(this);
      var head = $(this).find('thead');
      var width = obj.find('colgroup');
      var widthSum = 0;
      var height = '250px';
      var minItem = obj.data('minitem') ? obj.data('minitem') : 6;
      var cellHeight = obj.data('cellHeight');
      var flHeader = '';
      var scroll = false;


      //높이값
      if(minItem){
        if(!cellHeight) cellHeight = obj.find('table tbody tr > *').eq(0).outerHeight();
        
        if(minItem < obj.find('table tbody tr').length){
          height = cellHeight * minItem + 'px';
          scroll = true;
        }else{
          height = '';
        }
      }

      //header 생성
      head.find('tr > *').each(function(){
        flHeader += '<div>' + $(this).html() + '</div>';
      });

      flheader = $('<div class="floating-tb-header">'+ flHeader +'</div>');
      obj.prepend(flheader);
      
      obj.find('table').wrap('<div class="js__floatingHead_wrap" style="height:'+ height +';overflow-y:auto">');

      //스크롤생성
      if(scroll == true) scrollBind(obj.find('.js__floatingHead_wrap'));

      //퍼센트로 들어갈 column
      var peridx = width.find('col:not([class*=width])').index();

      width.find('col').each(function(){
        var col = $(this);
        var idx = col.index();

        if(col.attr('class')){
          flheader.find('> *').eq(idx).addClass(col.attr('class'));
          widthSum += Number(col.attr('class').replace(/[^0-9]/g,''));
        }else{
          // flheader.find('> *').eq(idx).css('width','calc(100% - '+ widthSum +'px)');
        }
      });

      flheader.find('> *').eq(peridx).css('width','calc(100% - '+ (widthSum) +'px)');

      // triggerScroll(obj);
    });
  }

  window.onload = function(){
    init();
  };

  // init();

});
jQuery(function($){
  var table = $('.js__tableFold');

  var getHeight = function(obj,optRows){
    //사이즈설정
    var cellHeight = 0;
    var ct = Number(optRows);
    var rowspan = 0;
    var rowspanct = 0;
    obj.find('> table > thead > tr, > table > tbody > tr').each(function(){
      var cell = $(this).find(' > *:first-child');

      if(cell.attr('rowspan')){
        rowspan = Number(cell.attr('rowspan'));
        ct += rowspan - 2;
        rowspanct = 0;
      }
      
      if(rowspanct < rowspan){
        if(rowspanct == rowspan){
          rowspanct = 0;
          rowspan = 0;
        }

        rowspanct++;

        if(rowspanct != 1) {
          return true
        }
      }

      cellHeight += cell.outerHeight();

      ct--;
      if(ct == 0){
        return false;
      }
    });

    return cellHeight - 1;
  }
  
  var btnTrigger = function(btn, obj, optRows){
    var btn = btn;
    var obj = obj;
    var sct = $(window).scrollTop();

    if(!btn.hasClass('active')){
      btn.addClass('active');
      obj.animate({'height':obj.find('table').height()-2});
      $('html,body').animate({'scrollTop': sct},'',function(){
        obj.css('height','');
      });
    }else{
      btn.removeClass('active');
      var cellHeight = obj.find('tr > *').eq(0).outerHeight();
      obj.animate({'height':getHeight(obj, optRows)});
    }

    //btn text
    if(btn.hasClass('active')){
      btn.find('strong').html('접어두기')
    }else{
      btn.find('strong').html('전체보기')
    }
  }

  var tableHeight = function(obj){
    var obj = obj;
    var optRows = obj.data('rows') ? obj.data('rows') : 3;

    obj.wrap('<div class="js__tableFold-wrap"></div>');
    var btn = $('<button class="js__tableFold-btn"><span></span><strong>전체보기</strong></button>');
    obj.after(btn);

    btn.on('click',function(){
      btnTrigger($(this),obj,optRows);
    });

    obj.css('height',getHeight(obj, optRows));

    
  }

  var init = function(){
    if(table.length < 1) return false;
    table.each(function(){
      tableHeight($(this));
    });
  }

  bindTableFoldAc = function(){
    init();
  }
});


jQuery(window).load(function($) {
  bindTableFoldAc();
});
jQuery(function($){
  var tabNavs = $('.js__tabNav');

  //초기에 보여질 tavnav content  calss active 추가
  var tabNavInit = function(obj){
    var className = obj.data('tabnav');
    $('*[class*='+ className +']').eq(0).addClass('active');
  }

  var tabNavBtn = function(btn){
    var obj = btn.parent();
    obj.parent().find('li').removeClass('active');
    obj.addClass('active');
  }

  var tabNav = function(){
    tabNavs.each(function(){
      tabNavInit($(this));

      var tabName = $(this).data('tabnav');
      $(this).find('button, a').on('click',function(){
        tabNavBtn($(this));
        var idx = $(this).data('idx');
        var className = 'js__tabNav_' + tabName;

        $('*[class *=' + className + ']').removeClass('active');
        $('.' + className + idx).addClass('active');
      });
    });
  }

  var init = function(){
    if(tabNavs.length < 1) return false;

    tabNavs.find('button, a').on('click',function(e){
      e.preventDefault();
    });
    
    // tabNavInit();
    tabNav();
  }

  init();
});
jQuery(function($){
  var target = $('.js__textCounter');
  var counterElm = '<span class="textlengthCounter"><span class="textCount">0</span>/<span class="textMax">50</span>Bytes</span>';

  
  var textCounter = function(){
    target.each(function(){
      var input = $(this);
      var maxByte = input.data('maxbyte') ? input.data('maxbyte') : 20;
      var count = $(counterElm);

      count.find('.textMax').html(numberFormat(maxByte));
      var textCount = count.find('.textCount');

      var text ='';

      //초기 글자수 체크
      textCount.html(numberFormat(getBytes($(this).val())));

      input.on('keyup keydown',function(){
        text = $(this).val();

        var bytes = getBytes(text);

        if(maxByte > bytes){
          textCount.html(numberFormat(bytes));
          count.find('.textCount').css('color','');
        }else{
          //글자수초가
          $(this).val(substrCut(text,maxByte));
          bytes = getBytes(text);
          textCount.html(numberFormat(bytes));
          count.find('.textCount').css('color','red');
        }
      });

      input.after(count);
    });
  }
  
  var init = function(){
    if(target.length < 1) return false;
    textCounter();
  }

  init();
});
jQuery(function($){
  var tipbox = $('.cs-box-tip');

  var tipboxInit = function(obj){
    var obj = obj;
    btn = $('<button type="button" class="tip-toggle-btn"></button>');
    
    obj.append(btn);

    btn.on('click',function(){
      obj.toggleClass('active');
    });
    // obj.appned('<button class="tip-toggle-btn"></button>');
  }
  
  var init = function(){
    if(tipbox.length < 1) return false;

    tipbox.each(function(){
      tipboxInit($(this));
    });
    
  }

  init();
});
//토글인데 토글이 안됨. ㅋㅋㅋㅋㅋ
//ver2.0을 마들때인듯함..
toggleElm = function(group,gidx,ac){
  var group = document.querySelectorAll('[data-group='+ group +']'),
      gidx = gidx ? gidx : 'all',
      ac = ac;

  group.forEach(function(obj,idx){
    data = obj.dataset;

    if(gidx == 'all'){
      obj.style.display = ac;
    }else if(data.idx == gidx){
      obj.style.display = ac;
    }
  });
}
//도면보기
jQuery(function($){
  blueprintFullJq = function(btn){
    var btn = $(btn);
    var ext = btn.data();
    var obj = btn.closest('.laymodal');

    if(!ext.fullscreen){
      blueprintFull(obj, btn);
    }else{
      blueprintZommOut(obj, btn);
    }
  }

  //전체화면
  var blueprintFull = function(obj, btn){
    obj.css({
      width :'100%',
    });

    obj.find('.blueprint-preview').css('height',$(window).height() - 345);

    btn.data('fullscreen','true');
    btn.text('팝업크기 축소');
  }

  //축소
  var blueprintZommOut = function(obj){
    btn = obj.find('.js__blueprintfull')

    obj.css({
      width : '',
    });

    obj.find('.blueprint-preview').css('height','');
    
    btn.data('fullscreen','');
    btn.text('전체 화면 보기');
  }

  blueprintCloseJq = function(btn){
    var btn = $(btn);
    var obj = btn.closest('.laymodal');
    
    blueprintZommOut(obj,btn);
    lay_pop_close(obj);
  }

  scrollBind($('.blueprint-list'));
});

function blueprintFull(btn){
  jQuery(function($){
    blueprintFullJq(btn);
  });
}

function blueprintClose(btn){
  jQuery(function($){
    blueprintCloseJq(btn);
  });
}